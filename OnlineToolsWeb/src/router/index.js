import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
	routes: [{
		path: '/',
		redirect: '/ToolsIndex'
	}, {
		path: '/',
		component: resolve => require(['../components/common/Home.vue'], resolve),
		meta: {
			title: '自述文件'
		},
		children: [{
			path: '/ToolsIndex',
			name: 'ToolsIndex',
			component: resolve => require(['../components/page/ToolsIndex.vue'], resolve),
			meta: {
				title: '工具首页'
			}
		}, {
			path: '/QRcodeGeneration',
			name: 'QRcodeGeneration',
			component: resolve => require(['../components/page/QRcodeGeneration.vue'], resolve),
			meta: {
				title: '条码/二维码生成'
			}
		}, {
			path: '/Base64Encoding',
			name: 'Base64Encoding',
			component: resolve => require(['../components/page/Base64Encoding.vue'], resolve),
			meta: {
				title: 'Base64编码/解码'
			}
		}, {
			path: '/Md5Encryption',
			name: 'Md5Encryption',
			component: resolve => require(['../components/page/Md5Encryption.vue'], resolve),
			meta: {
				title: 'MD5加密'
			}
		}, {
			path: '/SqlCodeFormat',
			name: 'SqlCodeFormat',
			component: resolve => require(['../components/page/SqlCodeFormat.vue'], resolve),
			meta: {
				title: '代码格式化'
			}
		}, {
			path: '/WordsNumber',
			name: 'WordsNumber',
			component: resolve => require(['../components/page/WordsNumber.vue'], resolve),
			meta: {
				title: '字数统计'
			}
		}, {
			path: '/JsonCodeFormat',
			name: 'JsonCodeFormat',
			component: resolve => require(['../components/page/JsonCodeFormat.vue'], resolve),
			meta: {
				title: 'JSON格式化整理'
			}
		}, {
			path: '/NumMoney',
			name: 'NumMoney',
			component: resolve => require(['../components/page/NumMoney.vue'], resolve),
			meta: {
				title: '数字转大写金额'
			}
		}],
	}, ]
})
