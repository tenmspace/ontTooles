export default {
	IsNumber: function(val) { //判断是否是数字
		var patrn = /^(-)?\d+(\.\d+)?$/;
		if (patrn.exec(value) == null || value == "") {
			return false
		} else {
			return true
		}
	},
	IsNumberByAlarm: function(val) { //判断是否是数字
		var patrn = /^(-)?\d+(\.\d+)?$/;
		if (patrn.exec(val) == null || val == "") {
			return false
		} else {
			return true
		}
	},
	IsNull: function(val) { //判断数据是否为空
		if (val == "" || val == null || typeof val == "undefined" || val == [] || val.length == 0) {
			return true;
		}
		return false;
	},
	ClearAllLocalSTorage: function() {
		localStorage.clear();
	},
	SetLocalStorage: function(key, value) { //存本地数据
		localStorage.setItem(key, value);
	},
	GetLocalStorage: function(key) { //根据key值取本地数据
		var value = localStorage.getItem(key);
		if (this.IsNull(value)) {
			value = "";
		}
		return value;
	},
	formatDate: function(date, fmt) {
		// debugger
		if (/(y+)/.test(fmt)) {
			fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
		}
		let o = {
			'M+': date.getMonth() + 1,
			'd+': date.getDate(),
			'h+': date.getHours(),
			'm+': date.getMinutes(),
			's+': date.getSeconds()
		};
		// debugger
		for (let k in o) {
			if (new RegExp(`(${k})`).test(fmt)) {
				let str = o[k] + '';
				fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? str : ('00' + str).substr(str.length));
			}
		}
		// debugger
		return fmt;
	},
	padLeftZero: function(str) {
		return ('00' + str).substr(str.length);
	},
}
